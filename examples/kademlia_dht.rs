use kademlia_dht::{Key, Node, SimpleStorage};
use sha3::{Digest, Sha3_256};
use std::thread;
use std::time::Duration;

fn clone_into_array<A, T>(slice: &[T]) -> A
where
    A: Sized + Default + AsMut<[T]>,
    T: Clone,
{
    let mut a = Default::default();
    <A as AsMut<[T]>>::as_mut(&mut a).clone_from_slice(slice);
    a
}

fn get_key(key: &str) -> Key {
    let mut hasher = Sha3_256::new();
    hasher.update(key.as_bytes());
    Key(clone_into_array(hasher.finalize().as_slice()))
}

fn main() {
    let mut node = Node::<SimpleStorage<String>, String, u64>::new(
        std::net::IpAddr::V4(std::net::Ipv4Addr::LOCALHOST),
        8080,
        Default::default(),
        None,
        None,
    );

    let key = get_key("Hello");
    let value = "World".to_owned();

    node.insert(key, &value, &0);

    // inserting is asynchronous, so sleep for a second
    thread::sleep(Duration::from_millis(1000));

    assert_eq!(node.get(&key).unwrap(), (value, 0));
}
