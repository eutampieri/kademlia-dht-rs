use crate::key::Key;
use crate::protocol::Value;
use crate::KEY_EXPIRATION;
use log::{info, log};
use std::collections::{BTreeMap, HashMap, HashSet};
use std::mem;
use std::time::{Duration, Instant};

pub trait Storage<V: Value, T: PartialEq + PartialOrd>: Clone + Send + Sync + 'static {
    type Error;
    /// Inserts an item into `Storage`.
    fn insert(&mut self, key: Key, value: V, timestamp: T) -> Result<(), Self::Error>;
    /// Returns the value associated with `key`. Returns `None` if such a key does not exist in
    /// `Storage`.
    fn get(&mut self, key: &Key) -> Option<(V, T)>;
}

/// A simple storage container that removes stale items.
///
/// `Storage` will remove a item if it is older than `KEY_EXPIRATION` seconds.
#[derive(Clone, Debug, Default)]
pub struct SimpleStorage<V> {
    items: HashMap<Key, (V, u64)>,
    publish_times: BTreeMap<u64, HashSet<Key>>,
}

impl<V> SimpleStorage<V> {
    /// Constructs a new, empty `Storage`.
    pub fn new() -> Self {
        Self {
            items: HashMap::new(),
            publish_times: BTreeMap::new(),
        }
    }

    /// Removes all items that are older than `KEY_EXPIRATION` seconds.
    fn remove_expired(&mut self) {
        let expiration_cutoff = std::time::SystemTime::now() - Duration::from_secs(KEY_EXPIRATION);
        let mut expired_times_map = self.publish_times.split_off(
            &expiration_cutoff
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        );
        mem::swap(&mut self.publish_times, &mut expired_times_map);

        for key in expired_times_map
            .into_iter()
            .flat_map(|entry| entry.1.into_iter())
        {
            info!("Removed {:?}", key);
            self.items.remove(&key);
        }
    }
}

impl<V: Value> Storage<V, u64> for SimpleStorage<V> {
    type Error = ();

    /// Inserts an item into `Storage`.
    fn insert(&mut self, key: Key, value: V, timestamp: u64) -> Result<(), ()> {
        self.remove_expired();
        let curr_time = std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .unwrap()
            .as_secs();

        if let Some(old_entry) = self.items.insert(key, (value, curr_time)) {
            if let Some(keys) = self.publish_times.get_mut(&old_entry.1) {
                keys.remove(&key);
            }
        }

        self.publish_times
            .entry(curr_time)
            .or_insert_with(HashSet::new)
            .insert(key);

        Ok(())
    }

    /// Returns the value associated with `key`. Returns `None` if such a key does not exist in
    /// `Storage`.
    fn get(&mut self, key: &Key) -> Option<(V, u64)> {
        self.remove_expired();
        self.items.get(key).map(|entry| entry.clone())
    }
}
