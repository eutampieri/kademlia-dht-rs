use log::{info, log};
use sha3::{Digest, Sha3_256};
use simplelog::{CombinedLogger, Config, Level, LevelFilter, TermLogger};
use std::convert::AsMut;
use std::io;
use std::{collections::HashMap, time::Instant};

use kademlia_dht::{Key, Node, SimpleStorage};

fn clone_into_array<A, T>(slice: &[T]) -> A
where
    A: Sized + Default + AsMut<[T]>,
    T: Clone,
{
    let mut a = Default::default();
    <A as AsMut<[T]>>::as_mut(&mut a).clone_from_slice(slice);
    a
}

fn get_key(key: &str) -> Key {
    let mut hasher = Sha3_256::new();
    hasher.update(key.as_bytes());
    Key(clone_into_array(hasher.finalize().as_slice()))
}

fn main() {
    CombinedLogger::init(vec![TermLogger::new(
        LevelFilter::Info,
        Config::default(),
        simplelog::TerminalMode::Mixed,
        simplelog::ColorChoice::Auto,
    )])
    .unwrap();

    let mut node_map = HashMap::new();
    let mut id = 0;
    for i in 0..50 {
        if i == 0 {
            let n = Node::<SimpleStorage<String>, String, u64>::new(
                std::net::IpAddr::V4(std::net::Ipv4Addr::LOCALHOST),
                8900 + i,
                Default::default(),
                None,
                None,
            );
            node_map.insert(id, n.clone());
        } else {
            let n = Node::new(
                std::net::IpAddr::V4(std::net::Ipv4Addr::LOCALHOST),
                8900 + i,
                Default::default(),
                Some(node_map[&0].node_data()),
                None,
            );
            node_map.insert(id, n.clone());
        }
        id += 1;
    }

    for i in (1..50).filter(|num| num % 10 == 0) {
        node_map[&i].kill();
    }

    let input = io::stdin();

    loop {
        let mut buffer = String::new();
        println!("Ready for input!");
        if input.read_line(&mut buffer).is_err() {
            break;
        }
        let args: Vec<&str> = buffer.trim_end().split(' ').collect();
        match args[0] {
            "new" => {
                let index: u16 = args[1].parse().unwrap();
                let node = Node::new(
                    std::net::IpAddr::V4(std::net::Ipv4Addr::LOCALHOST),
                    8900 + id,
                    Default::default(),
                    Some(node_map[&index].node_data()),
                    None,
                );
                node_map.insert(id, node);
                id += 1;
            }
            "insert" => {
                let index: u16 = args[1].parse().unwrap();
                let key = get_key(args[2]);
                let value = args[3];
                node_map
                    .get_mut(&index)
                    .unwrap()
                    .insert(key, &value.to_string(), &0);
            }
            "get" => {
                let index: u16 = args[1].parse().unwrap();
                let key = get_key(args[2]);
                info!("{:?}", node_map.get_mut(&index).unwrap().get(&key));
            }
            _ => {}
        }
    }
}
