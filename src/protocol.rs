use crate::key::Key;
use crate::node::node_data::NodeData;
use bincode;
use bincode::Options;
use log::{log, warn};
use serde_derive::{Deserialize, Serialize};
use std::net::UdpSocket;
use std::str;
use std::sync::mpsc::Sender;
use std::sync::Arc;
use std::thread;

/// An enum representing a request RPC.
///
/// Each request RPC also carries a randomly generated key. The response to the RPC must contain
/// the same randomly generated key or else it will be ignored.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Request<V, T> {
    pub id: Key,
    pub sender: NodeData,
    pub payload: RequestPayload<V, T>,
}

impl<V, T> Request<V, T> {
    fn update_node_address(&mut self, address: std::net::SocketAddr) {
        self.sender.addr = (address.ip(), address.port())
    }
}

/// An enum representing the payload to a request RPC.
///
/// As stated in the Kademlia paper, the four possible RPCs are `PING`, `STORE`, `FIND_NODE`, and
/// `FIND_VALUE`.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum RequestPayload<V, T> {
    Ping,
    Store(Key, V, T),
    FindNode(Key),
    FindValue(Key),
    Custom(Vec<u8>),
    LastUpdated(Key),
}

/// An enum representing the response to a request RPC.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Response<V, T> {
    pub request: Request<V, T>,
    pub receiver: NodeData,
    pub payload: ResponsePayload<V, T>,
}

impl<V, T> Response<V, T> {
    fn update_node_address(&mut self, address: std::net::SocketAddr) {
        self.receiver.addr = (address.ip(), address.port())
    }
}

/// An enum representing the payload to a response RPC.
///
/// As stated in the Kademlia paper, a response to a request could be a list of nodes, a value, or
/// a pong.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ResponsePayload<V, T> {
    Nodes(Vec<NodeData>),
    Value(V, T),
    Pong,
    Custom(Vec<u8>),
    LastUpdated(T),
}

/// An enum that represents a message that is sent between nodes.
#[derive(Serialize, Deserialize, Debug)]
pub enum Message<V, T> {
    Request(Request<V, T>),
    Response(Response<V, T>),
    Kill,
}

impl<V, T> Message<V, T> {
    fn update_node_address(&mut self, address: std::net::SocketAddr) {
        match self {
            Message::Request(r) => r.update_node_address(address),
            Message::Response(r) => r.update_node_address(address),
            Message::Kill => {}
        }
    }
}

/// `Protocol` facilitates the underlying communication between nodes by sending messages to other
/// nodes, and by passing messages from other nodes to the current node.
#[derive(Clone, Debug)]
pub struct Protocol<V, T> {
    socket: Arc<UdpSocket>,
    _phantom: std::marker::PhantomData<(V, T)>,
}

impl<V: Value, T: Timestamp> Protocol<V, T> {
    pub fn new(socket: UdpSocket, tx: Sender<Message<V, T>>) -> Protocol<V, T> {
        let protocol = Protocol::<V, T> {
            socket: Arc::new(socket),
            _phantom: std::marker::PhantomData,
        };
        let ret = protocol.clone();
        thread::spawn(move || {
            let mut buffer = vec![];
            loop {
                let mut len_buf = [0; (usize::BITS / 8) as usize];
                protocol.socket.peek(&mut len_buf).unwrap();
                let len = u64::from_le_bytes(len_buf);
                buffer.resize(
                    ((usize::BITS / 8) as u64 + len % usize::MAX as u64) as usize,
                    0,
                );
                let (len, address) = protocol.socket.recv_from(&mut buffer).unwrap();
                if len == 0 {
                    continue;
                }
                let mut message: Message<V, T> = bincode::DefaultOptions::default()
                    .deserialize(&buffer[((usize::BITS / 8) as usize)..len])
                    .unwrap();

                message.update_node_address(address);

                if tx.send(message).is_err() {
                    warn!("Protocol: Connection closed.");
                    break;
                }
            }
        });
        ret
    }

    pub fn send_message(&self, message: &Message<V, T>, node_data: &NodeData) {
        let serializer = bincode::config::DefaultOptions::new();
        let mut buffer = serializer.serialize(&message).unwrap();
        let buffer = {
            let mut out = (buffer.len() as u64).to_le_bytes().to_vec();
            out.append(&mut buffer);
            out
        };
        let NodeData { ref addr, .. } = node_data;
        if self.socket.send_to(&buffer, addr).is_err() {
            warn!("Protocol: Could not send data.");
        }
    }
}

pub trait Value:
    Clone + Send + for<'a> serde::Deserialize<'a> + serde::Serialize + Sync + std::fmt::Debug + 'static
{
}

pub trait Timestamp:
    Clone
    + Send
    + for<'a> serde::Deserialize<'a>
    + serde::Serialize
    + Sync
    + std::fmt::Debug
    + 'static
    + PartialEq
    + PartialOrd
{
}

impl<
        T: Clone
            + Send
            + for<'a> serde::Deserialize<'a>
            + serde::Serialize
            + Sync
            + std::fmt::Debug
            + 'static
            + PartialEq
            + PartialOrd,
    > Timestamp for T
{
}

impl<
        T: Clone
            + Send
            + for<'a> serde::Deserialize<'a>
            + serde::Serialize
            + Sync
            + std::fmt::Debug
            + 'static,
    > Value for T
{
}
